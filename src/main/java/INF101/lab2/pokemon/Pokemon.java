package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {

    // Feltvariabler 
    String name;  //Navnet til Pokémon'en
    int healthPoints; //Health points er hvor mye liv Pokémon'en har
    int maxHealthPoints; //Hvor mange health points Pokémon'en kan maksimalt ha (og starter med)
    int strength; //Hvor sterk Pokémon'en er
    Random random; /*
     I Pokémon er det noenlunde tilfeldig hvor mye man skader fienden i et angrep.
     For å kunne ha denne tilfeldigheten skal vi bruke et Random-objekt 
    */ 

    /*
     * Konstruktøren i en klasse er en metode som oppretter et objekt av den datatypen. 
     * TODO: Implementer en konstruktør som tar inn ett argument: String name. Konstruktøren skal initiere feltvariablene. 
     * maxHealthPoints, healthPoints og strength skal gis verdier basert på tilfeldighet. Bruk de følgende linjene:
    */

    public Pokemon(String name){
        this.name = name; 
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
    }

    /*
     * Implementer metodene getName, getStrength, getCurrentHP, getMaxHP og isAlive. 
     * Les dokumentasjonen til metodene for å vite hva de skal gjøre.
    */

    public String getName() {
        return this.name; 
    }

    @Override
    public int getStrength() {
        return this.strength;
    }

    @Override
    public int getCurrentHP() {
        return this.healthPoints;
    }

    @Override
    public int getMaxHP() {
        return this.maxHealthPoints;
    }

    public boolean isAlive() {
        return this.healthPoints > 0;
    }

    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());

        System.out.println(this.name + " attacks " + target.getName());

        target.damage(damageInflicted);
    

        if (target.isAlive() == false){ //
            System.out.println(target.getName() + " is defeated by " + this.name);

        }
    }

    @Override
    public void damage(int damageTaken) {
        if (damageTaken < 0){ //Det skal ikke være mulig å gi negative skade, altså øke antall health points.
            System.out.println("You can´t do negative damage!");
            return; 
        } 
        else if ((this.healthPoints - damageTaken) > 0){ // Hvis pokemon tar damage 
            this.healthPoints -= damageTaken; //Metoden skal trekke i fra damageTaken fra antall healthPoints Pokémon'en har. 
        }
        else {
            this.healthPoints = 0; //
        }

        System.out.println(this.name + " takes " + damageTaken + " damage and is left with " + this.healthPoints + "/" + this.maxHealthPoints + " HP");

    }

    @Override
    public String toString() {
        String info = ""; //
        info += this.name + " HP: (" + this.healthPoints + "/" + this.maxHealthPoints + ") STR: " + this.strength;

        return info;
    }

}
